package club.qweqwe.exception;

public class ExceptionObject {
    private String message;
    private int code;

    public ExceptionObject(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}
