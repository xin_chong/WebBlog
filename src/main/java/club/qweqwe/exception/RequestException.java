package club.qweqwe.exception;

public class RequestException extends BaseException {
    public final static RequestException REQUEST_ERROR_CODE_PARAMER = new RequestException("请求参数异常!", 1001);
    public final static RequestException REQUEST_ERROR_CODE_REGISTER_FORM_INFO = new RequestException("注册信息无效!", 1002);
    public final static RequestException REQUEST_ERROR_CODE_LOGIN_FORM_INFO = new RequestException("登陆信息无效!", 1003);
    public final static RequestException REQUEST_ERROR_CODE_LOGIN_DATA_INFO = new RequestException("帐号或密码无效!", 1004);
    public final static RequestException REQUEST_ERROR_CODE_LOGIN_REGISTER = new RequestException("登陆状态下注册!", 1005);

    public final static RequestException LOGIN_SUCCESSFUL = new RequestException("登陆成功!", 1006);
    public final static RequestException LOGOUT_SUCCESSFUL = new RequestException("注销成功!", 1007);
    public final static RequestException LOGOUT_ERROR = new RequestException("注销未知异常!", 1008);


    public RequestException(String message, int code) {
        super(message, code);
    }
}
