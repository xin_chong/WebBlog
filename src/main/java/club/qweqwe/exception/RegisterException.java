package club.qweqwe.exception;

public class RegisterException extends BaseException {
    public static final RegisterException REGISTER_ERROR_EXCEPTION = new RegisterException("帐号注册失败!", 2001);
    public static final RegisterException REGISTER_SUCCESSFUL = new RegisterException("注册成功!", 2002);

    public RegisterException() {
    }

    public RegisterException(String message, int code) {
        super(message, code);
    }
}
