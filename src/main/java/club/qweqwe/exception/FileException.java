package club.qweqwe.exception;

import com.alibaba.fastjson.JSON;

import java.io.File;
import java.util.List;

public class FileException extends BaseException {

    public static final FileException FILE_ERROR_CODE_VALID_LOGIN = new FileException("用户没有登陆，无法上传!", 3001);
    public static final FileException FILE_ERROR_CODE_UPLOAD_FILE = new FileException("上传失败，请再尝试!", 3001);

    private List<String> fileNames;

    public List<String> getFileNames() {
        return fileNames;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }

    public FileException() {
    }

    public FileException(String message, int code) {
        super(message, code);
    }

    private class FileExceptionObject extends ExceptionObject{

        private List<String> fileNames;

        public List<String> getFileNames() {
            return fileNames;
        }

        public void setFileNames(List<String> fileNames) {
            this.fileNames = fileNames;
        }

        public FileExceptionObject(String message, int code, List<String> filenames) {
            super(message, code);
            this.fileNames = filenames;
        }
    }

    public FileException(FileException e) {
        super(e.getMessage(), e.getCode());
        setFileNames(e.getFileNames());
    }

    @Override
    public String toJsonString() {
        if(fileNames == null)
            return super.toJsonString();

        FileExceptionObject object = new FileExceptionObject(this.getMessage(), getCode(), getFileNames());
        return JSON.toJSONString(object); //序列化
    }
}
