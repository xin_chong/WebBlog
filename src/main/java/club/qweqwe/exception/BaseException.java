package club.qweqwe.exception;

import com.alibaba.fastjson.JSON;

public class BaseException extends RuntimeException{

    private int code = 0; // 未知错误

    public int getCode() {
        return code;
    }

    public BaseException() {
        super("未知错误");
    }

    public BaseException(String message, int code) {
        super(message);
        this.code = code;
    }

    public String toJsonString() {
        ExceptionObject object = new ExceptionObject(this.getMessage(), getCode());
        return JSON.toJSONString(object); //序列化
    }
}
