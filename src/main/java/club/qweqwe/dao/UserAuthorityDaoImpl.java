package club.qweqwe.dao;

import club.qweqwe.dao.impl.BasicDaoImpl;
import club.qweqwe.model.dbobj.UserAuthority;
import club.qweqwe.model.form.Register;
import club.qweqwe.utils.ReflectUtils;

import javax.sql.DataSource;

public class UserAuthorityDaoImpl extends BasicDaoImpl<UserAuthority> {
    public UserAuthorityDaoImpl() {
    }

    public UserAuthorityDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    public void insert(UserAuthority authority){
        String tableName = getTypeName();
        String sql = String.format("INSERT INTO %s (id, authority) VALUE(?, ?);", tableName);
        insert(sql, authority.getId(), authority.getAuthority());
    }

    public UserAuthority register(String uuid, Register register){
        UserAuthority authority = new UserAuthority();
        authority.setId(uuid);
        insert(authority);
        return authority;
    }

    public UserAuthority query(String uuid){
        String tableName = ReflectUtils.getClassName(ReflectUtils.getSuperGenericType(getClass()));
        String sql = String.format("SELECT * FROM %s WHERE id = ?;", tableName);
        return query(sql, uuid);
    }
}
