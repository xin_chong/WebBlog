package club.qweqwe.dao;

import club.qweqwe.dao.impl.BasicDaoImpl;
import club.qweqwe.model.dbobj.UserInfo;
import club.qweqwe.model.form.Register;
import club.qweqwe.utils.ReflectUtils;

import javax.sql.DataSource;
import java.sql.Timestamp;

public class UserInfoDaoImpl extends BasicDaoImpl<UserInfo> {
    public UserInfoDaoImpl() {
    }

    public UserInfoDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    public void insert(UserInfo info){
        String tableName = getTypeName();
        String sql = String.format("INSERT INTO %s (id, name, gender, age, registerTime, email, avatar) VALUE(?, ?, ?, ?, ?, ?, ?);", tableName);
        insert(sql, info.getId(), info.getName(), (short)info.getGender(), info.getAge(), info.getRegisterTime(), info.getEmail(), info.getAvtar());
    }

    public UserInfo register(String uuid, Register register){
        UserInfo info = new UserInfo();
        info.setId(uuid);
        info.setAge(register.getAge().shortValue());
        info.setEmail(register.getEmail());
        info.setGender(register.getGender());
        info.setName(register.getName());
        info.setRegisterTime(new Timestamp(System.currentTimeMillis()));
        info.setAvtar(register.getAvatar());
        insert(info);
        return info;
    }

    public UserInfo query(String uuid){
        String tableName = ReflectUtils.getClassName(ReflectUtils.getSuperGenericType(getClass()));
        String sql = String.format("SELECT * FROM %s WHERE id = ?;", tableName);
        return query(sql, uuid);
    }
}
