package club.qweqwe.dao;

import club.qweqwe.utils.ReflectUtils;

import java.util.List;

public abstract class DAO<T> {
    public abstract int update(String sql, Object ... objects);

    public abstract T query(String sql, Object ... objects);

    public abstract int insert(String sql, Object ... objects);

    public abstract int delete(String sql, Object ... objects);

    public abstract List<T> querySet(String sql, Object ... objects);

    public String getTypeName(){
        return ReflectUtils.getClassName(ReflectUtils.getSuperGenericType(getClass()));
    }
}
