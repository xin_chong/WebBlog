package club.qweqwe.dao;

import club.qweqwe.dao.impl.BasicDaoImpl;
import club.qweqwe.model.dbobj.FileInfo;

import javax.sql.DataSource;

public class FileLogDaoImpl extends BasicDaoImpl<FileInfo> {
    public FileLogDaoImpl() {
    }

    public FileLogDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    public boolean insert(FileInfo fileInfo){
        String sql = String.format("INSERT INTO %s (id, fileName, realFileName) VALUE(?, ?, ?);", getTypeName());
        int result = insert(sql, fileInfo.getId(), fileInfo.getFileName(), fileInfo.getRealFileName());
        return result > 0;
    }

    public boolean addFile(String uuid, String fileName, String realFileName){
        FileInfo fileInfo = new FileInfo(uuid, fileName, realFileName);
        return insert(fileInfo);
    }
}
