package club.qweqwe.filter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CharsetFilter extends HttpFilter {
    private String charset;

    @Override
    public void init() throws ServletException {
        charset = getFilterConfig().getServletContext().getInitParameter("Charset");
    }

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(charset != null) {
            request.setCharacterEncoding(charset);
            response.setCharacterEncoding(charset);
        }

        super.doFilter(request, response, chain);
    }
}
