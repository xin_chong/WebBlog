package club.qweqwe.filter;

import club.qweqwe.GlobalConfig;
import club.qweqwe.exception.RequestException;
import club.qweqwe.model.form.Register;
import club.qweqwe.utils.ReflectUtils;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class RegisterFilter extends BaseFilter {
    @Override
    protected void doFilterEx(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException, IllegalAccessException ,InvocationTargetException {
        String methodURI = request.getParameter(GlobalConfig.REQUEST_URI_METHOD_NAME);
        String clazzName = ReflectUtils.getClassName(Register.class);
        if (methodURI != null && clazzName.compareToIgnoreCase(methodURI) == 0) {
            Map<String, String> map = ReflectUtils.getFormBeanRequestParams(Register.class, request);
            Register register = new Register();
            BeanUtils.populate(register, map);
            if (!register.isValid())
                throw RequestException.REQUEST_ERROR_CODE_REGISTER_FORM_INFO;

            request.setAttribute(clazzName, register);
        }

        chain.doFilter(request, response);
    }
}
