package club.qweqwe.filter;

import club.qweqwe.exception.FileException;
import club.qweqwe.model.dbobj.User;
import club.qweqwe.utils.ReflectUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

@WebFilter(filterName = "FileFilter")
public class FileFilter extends BaseFilter {

    @Override
    protected void doFilterEx(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException, IllegalAccessException, InvocationTargetException {
        HttpSession session = request.getSession(false);
        if (session == null)
            throw FileException.FILE_ERROR_CODE_VALID_LOGIN;

        User user = (User) session.getAttribute(ReflectUtils.getClassName(User.class));
        if (user == null) {
            session.setMaxInactiveInterval(0);
            throw FileException.FILE_ERROR_CODE_VALID_LOGIN;
        }

        chain.doFilter(request, response);
    }
}
