package club.qweqwe.model.dbobj;

import club.qweqwe.annotation.ClassMeta;

import java.util.Objects;

@ClassMeta(className = "t_user_authority")
public class UserAuthority {
    private String id;
    private long authority = 0;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAuthority that = (UserAuthority) o;
        return authority == that.authority &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, authority);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserAuthority() {
    }

    public long getAuthority() {
        return authority;
    }

    public void setAuthority(long authority) {
        this.authority = authority;
    }

    public UserAuthority(String id, long authority) {
        this.id = id;
        this.authority = authority;
    }
}
