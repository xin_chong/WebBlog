package club.qweqwe.model.dbobj;

import club.qweqwe.annotation.ClassMeta;

import java.sql.Timestamp;
import java.util.Objects;

@ClassMeta(className = "t_user_info")
public class UserInfo {
    private String id;
    private String name;
    private char gender;
    private short age;
    private Timestamp registerTime;
    private String email;
    private String avtar;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfo userInfo = (UserInfo) o;
        return gender == userInfo.gender &&
                age == userInfo.age &&
                Objects.equals(id, userInfo.id) &&
                Objects.equals(name, userInfo.name) &&
                Objects.equals(registerTime, userInfo.registerTime) &&
                Objects.equals(email, userInfo.email) &&
                Objects.equals(avtar, userInfo.avtar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, gender, age, registerTime, email, avtar);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public short getAge() {
        return age;
    }

    public void setAge(short age) {
        this.age = age;
    }

    public Timestamp getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Timestamp registerTime) {
        this.registerTime = registerTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvtar() {
        return avtar;
    }

    public void setAvtar(String avtar) {
        this.avtar = avtar;
    }

    public UserInfo() {
    }

    public UserInfo(String id, String name, char gender, short age, Timestamp registerTime, String email, String avtar) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.registerTime = registerTime;
        this.email = email;
        this.avtar = avtar;
    }
}
