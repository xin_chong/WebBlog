package club.qweqwe.model.dbobj;

import club.qweqwe.annotation.ClassMeta;

import java.util.Objects;

@ClassMeta(className = "t_file_log")
public class FileInfo {
    String id; //用户ID
    String fileName; //文件名
    String realFileName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileInfo fileinfo = (FileInfo) o;
        return Objects.equals(id, fileinfo.id) &&
                Objects.equals(fileName, fileinfo.fileName) &&
                Objects.equals(realFileName, fileinfo.realFileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fileName, realFileName);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRealFileName() {
        return realFileName;
    }

    public void setRealFileName(String realFileName) {
        this.realFileName = realFileName;
    }

    public FileInfo() {
    }

    public FileInfo(String id, String fileName, String realFileName) {
        this.id = id;
        this.fileName = fileName;
        this.realFileName = realFileName;
    }
}
