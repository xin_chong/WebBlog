package club.qweqwe.model.form;

import club.qweqwe.annotation.ClassMeta;
import club.qweqwe.annotation.NotNull;
import club.qweqwe.annotation.StrlenLimit;
import club.qweqwe.utils.ReflectUtils;

@ClassMeta(className = "login")
public class Login implements Checker {

    @NotNull
    @StrlenLimit(min = 6, max = 32)
    private String account;

    @NotNull
    @StrlenLimit(min = 32, max = 32)
    private String password;

    private String captcha;

    public Login(String account, String password, String captcha) {
        this.account = account;
        this.password = password;
        this.captcha = captcha;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public Login() {
    }
}
