package club.qweqwe.model.form;

import club.qweqwe.utils.ReflectUtils;

public interface Checker {
    default boolean isValid() {
        return ReflectUtils.annotationChecker(this.getClass(), this);
    }
}
