package club.qweqwe.model.form;

import club.qweqwe.annotation.*;

@ClassMeta(className = "register")
public class Register implements Checker {

    @NotNull
    @StrlenLimit(min = 6, max = 32)
    private String name;

    @NotNull
    @CharLimit({'M', 'F'})
    private Character gender;

    @NotNull
    @IntegerLimit(min = 0, max = Short.MAX_VALUE)
    private Integer age;

    @NotNull
    @StrlenLimit(max = 36)
    private String avatar;//头像地址

    @NotNull
    @StrlenLimit(min = 6, max = 32)
    private String account;

    @NotNull
    @StrlenLimit(min = 32, max = 32)
    private String password;

    @NotNull
    @StrlenLimit(max = 64)
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Register(String name, Character gender, Integer age, String avatar, String account, String password, String email) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.avatar = avatar;
        this.account = account;
        this.password = password;
        this.email = email;
    }

    public Register() {
    }
}
