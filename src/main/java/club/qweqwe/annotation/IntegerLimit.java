package club.qweqwe.annotation;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IntegerLimit {
    int min() default Integer.MIN_VALUE;
    int max() default Integer.MAX_VALUE;
}
