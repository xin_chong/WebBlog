package club.qweqwe.servlet;

import club.qweqwe.dao.UserAuthorityDaoImpl;
import club.qweqwe.dao.UserDaoImpl;
import club.qweqwe.dao.UserInfoDaoImpl;
import club.qweqwe.exception.RegisterException;
import club.qweqwe.exception.RequestException;
import club.qweqwe.listener.DatabaseConfiglistener;
import club.qweqwe.model.dbobj.User;
import club.qweqwe.model.dbobj.UserAuthority;
import club.qweqwe.model.dbobj.UserInfo;
import club.qweqwe.model.form.Login;
import club.qweqwe.model.form.Register;
import club.qweqwe.utils.ReflectUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet(name = "UserServlet")
public class UserServlet extends HttpServlet implements ServletHelper {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dispatcher(request,response, "Post");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dispatcher(request, response, "Get");
    }

    public void registerPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if(session != null){
            User user = (User) session.getAttribute(ReflectUtils.getClassName(User.class));
            if(user != null){
                throw RequestException.REQUEST_ERROR_CODE_LOGIN_REGISTER;
            }
        }

        String clazzName = ReflectUtils.getClassName(Register.class);
        Register register = (Register) request.getAttribute(clazzName);
        DataSource dataSource = (DataSource) request.getServletContext().getAttribute(DatabaseConfiglistener.DATABASE_KEY);

        //1.注册用户
        UserDaoImpl userDao = new UserDaoImpl(dataSource);
        User user = userDao.register(register);
        if(user == null)
            throw RegisterException.REGISTER_ERROR_EXCEPTION;

        String uuid = user.getId();

        //2.添加信息
        UserInfoDaoImpl userInfoDao = new UserInfoDaoImpl(dataSource);
        UserInfo userInfo = userInfoDao.register(uuid, register);
        //3.添加权限
        UserAuthorityDaoImpl userAuthorityDao = new UserAuthorityDaoImpl(dataSource);
        UserAuthority authority = userAuthorityDao.register(uuid, register);

        session = request.getSession(true);
        session.setAttribute(ReflectUtils.getClassName(User.class), user);
        session.setAttribute(ReflectUtils.getClassName(UserInfo.class), userInfo);
        session.setAttribute(ReflectUtils.getClassName(UserAuthority.class), authority);
        throw RegisterException.REGISTER_SUCCESSFUL;
    }

    public void loginPost(HttpServletRequest request, HttpServletResponse response){
        String clazzName = ReflectUtils.getClassName(Login.class);
        Login login = (Login) request.getAttribute(clazzName);
        DataSource dataSource = (DataSource) request.getServletContext().getAttribute(DatabaseConfiglistener.DATABASE_KEY);

        //1.查询用户
        UserDaoImpl userDao = new UserDaoImpl(dataSource);
        User user = userDao.query(login.getAccount(), login.getPassword());
        if(user == null)
            throw RequestException.REQUEST_ERROR_CODE_LOGIN_DATA_INFO;
        //2.查询信息

        UserInfoDaoImpl userInfoDao = new UserInfoDaoImpl(dataSource);
        UserInfo userInfo = userInfoDao.query(user.getId());

        //3.查询权限
        UserAuthorityDaoImpl userAuthorityDao = new UserAuthorityDaoImpl(dataSource);
        UserAuthority authority = userAuthorityDao.query(user.getId());

        //设置seesion
        HttpSession session = request.getSession(true);
        session.setAttribute(ReflectUtils.getClassName(User.class), user);
        session.setAttribute(ReflectUtils.getClassName(UserInfo.class), userInfo);
        session.setAttribute(ReflectUtils.getClassName(UserAuthority.class), authority);
        throw RequestException.LOGIN_SUCCESSFUL;
    }

    public void logoutPost(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession(false);
        if(session == null)
            throw RequestException.LOGOUT_ERROR;

        session.setMaxInactiveInterval(0);
        throw RequestException.LOGOUT_SUCCESSFUL;
    }

}
