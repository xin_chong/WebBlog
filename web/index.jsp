<%@ page import="club.qweqwe.utils.ReflectUtils" %>
<%@ page import="club.qweqwe.model.dbobj.UserInfo" %><%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false" %>
<html>
  <head>
    <title></title>
  </head>
  <body>
    <%

      HttpSession session = request.getSession(false);
      if(session != null){
          UserInfo info = (UserInfo) session.getAttribute(ReflectUtils.getClassName(UserInfo.class));
          out.println(info.getEmail());
          out.println(info.getAge());
          out.println(info.getName());
          out.println(info.getRegisterTime());
      }

    %>
  </body>
</html>