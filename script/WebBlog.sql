CREATE TABLE if not exists t_user(
  id char(36) PRIMARY KEY,
  account varchar(32) not null unique key,
  password varchar(32) not null
);

create table if not exists t_user_authority(
  id char(36) primary key ,
  authority char not null default 'F',
  constraint authority_fk_user foreign key(id) references t_user(id)
)

create table if not exists t_user_info(
   id char(36) primary key,
   name varchar(32) not null,
   gender char not null default 'M',
   age smallint not null,
   registerTime TIMESTAMP not null,
   email varchar(64) not null,
   avatar varchar(36) not null,
   constraint info_fk_user foreign key(id) references t_user(id)
)

create table if not exists t_article(
  id int primary key auto_increment,
  content text not null
)

create table if not exists t_article_info(
  id int primary key,
  createTime TIMESTAMP not null default current_timestamp ,
  lastChangeTime TIMESTAMP not null default current_timestamp ,
  type TINYINT not null,
  releaseTime timestamp not null default current_timestamp ,
  releaseState TINYINT not null,
  constraint info_fk_article foreign key(id) references t_article(id)
)

create table if not exists t_article_relational(
  id int,
  userId char(36),
  constraint primary key(id, userId),
  constraint article_fk_relational foreign key(id) references t_article(id),
  constraint article_fk_user_relational foreign key(userId) references t_user(id)
)

create table if not exists t_left_messages(
  id int primary key auto_increment,
  message text not null
)

create table if not exists t_left_messages_info(
  id int primary key,
  title varchar(128) not null,
  joinTime TIMESTAMP not null,
  constraint info_fk_left_messages foreign key(id) references t_left_messages(id)
)

create table if not exists t_left_messages_relational(
  id int ,
  articleId int,
  constraint primary key (id, articleId),
  constraint relational_rk_left_messages foreign key(id) references t_left_messages(id),
  constraint relational_rk_article foreign key(articleId) references t_article(id)
)

create table if not exists t_website_info(
  k text not null,
  v text not null)